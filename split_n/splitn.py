# 把字符串 n 等分
split_n = lambda s,n:[s[i:i+n] for i in range(0,len(s),n)]

l = split_n('aaabbbcccddd',3)
for s in l:
    print(s)
