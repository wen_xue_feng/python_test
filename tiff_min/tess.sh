#!/usr/bin/env bash
export TESSDATA_PREFIX=/usr/share/tesseract-ocr/tessdata
tesseract tif-0.tiff stdout -l number > result.txt
n=1
while [ -f tif-$n.tiff ]; 
do 
    tesseract tif-$n.tiff stdout -l number >> result.txt
    let n+=1 
done
