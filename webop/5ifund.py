from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time,sys
from lxml import etree

def parse_html(html):
    res = []
    dom = etree.HTML( html )
    date1 = dom.xpath('//span[@rel="star"]')
    res.append('# '+date1[0].text+'\n')
    funds = dom.xpath('//tbody/tr')
    #[@class="odd"]
    print('total:',len(funds))
    for f in funds:
        link_hm = f.find('td/a[@field="code"]')
        jjhm = link_hm.text
        href = link_hm.get('href')
        name = f.find('td/a[@field="name"]').text
        jz = f.find('td[@field="net"]').text
        ljjz = f.find('td[@field="totalnet"]').text
        try:
            res.append('- %s %s http://fund.eastmoney.com/%s %s %s\n'%(jjhm,name,href,eval(jz),eval(ljjz)))
        except:
            pass
    return res

def save_data( html ):
    ret = parse_html(html)
    print('used:',len(ret))
    fp = open('output/5ifund.md','w')
    for item in ret:
        fp.write(item)
    fp.close()

def test(filename):
    data = ''
    fp = open(filename,'r')
    for dat in fp:
        data = data + dat
    fp.close()
    save_data( data )
    
#test('5ifund.htm')
#sys.exit(0)

from pyvirtualdisplay import Display
display = Display(visible=0,size=(800,600))
display.start()
try:
    url1='http://fund.10jqka.com.cn/datacenter/jz/'
    #content: tbody id="containerMain"
    #div class="cont_items"
    #footer: div class="footer"
    driver = webdriver.Chrome(r'/usr/lib/chromium-browser/chromedriver')
    driver.get( url1 )
    time.sleep(2)
    posY = 0
    while True:
        element = driver.find_element_by_xpath('//div[@class="footer"]')
        pos1 = element.location
        if pos1['y'] == posY:
            break
        posY = pos1['y']
        pos2 = element.location_once_scrolled_into_view
        print( pos1 )
        time.sleep(0.5)
    #element = WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.XPATH, '//div[@class="cont_items"]')))
    element = driver.find_element_by_xpath('//div[@class="cont_items"]')
    save_data( element.get_attribute('innerHTML') )
    print('data saved')
    driver.quit()
except Exception as e:
    print(e)

display.stop()
