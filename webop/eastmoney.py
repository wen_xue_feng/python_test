from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
#from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time,os,sys
from lxml import etree

def parse_html(html):
    res = []
    dom = etree.HTML( html )
    date1 = dom.xpath('//tr[@class="h"]/td/span[1]')
    res.append('# '+date1[0].text+'\n')
    funds = dom.xpath('//tbody/tr')
    #[@class="odd"]
    print('total:',len(funds))
    for f in funds:
        jjhm = f.find('td[@class="bzdm"]').text
        link = f.find('td/nobr/a')
        jz = f.find('td[@class="dwjz black"]').text
        ljjz = f.find('td[@class="ljjz black"]').text
        try:
            res.append('- %s %s http://fund.eastmoney.com/%s %s %s\n'%(jjhm,link.get('title'),link.get('href'),eval(jz),eval(ljjz)))
        except:
            pass
    return res

def save_data( html ):
    ret = parse_html(html)
    print('used:',len(ret))
    fp = open('output/eastmoney.md','w')
    for item in ret:
        fp.write(item)
    fp.close()

from pyvirtualdisplay import Display
display = Display(visible=0,size=(800,600))
display.start()
try:
    url='http://fund.eastmoney.com/fund.html#os_0;isall_0;ft_;pt_1'
    #div ID 'tableDiv'
    #table ID 'oTable'
    #link_text '一键查看全部'
    #link ID 'checkall'
    #净值 http://fund.eastmoney.com/f10/jjjz_001626.html
    #换页链接 ID pagebar

    driver = webdriver.Chrome(r'/usr/lib/chromium-browser/chromedriver')
    driver.get( url )
    r = WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.ID, "checkall")))
    pos = r.location_once_scrolled_into_view
    r.click()

    time.sleep(2)
    element = driver.find_element_by_id( "tableDiv")
    #fp=open('eastmoney.htm','w')
    #fp.write( element.get_attribute('innerHTML') )
    #fp.close()
    save_data(element.get_attribute('innerHTML') )
    print('data saved')
    driver.quit()
except Exception as e:
    print(e)
display.stop()
