#!/usr/bin/env python3
from PIL import ImageFont,Image,ImageDraw
import sys

def gen_icon( ch ):
    truetype_path='/usr/share/fonts/truetype/ttf-bitstream-vera/Vera.ttf'
    font = ImageFont.truetype(font=truetype_path,size=50)
    #汉字
    #width = 60
    #height = 60
    #left = 5
    #英文
    w,h = font.getsize(ch)
    print(ch,w,h)
    width = w+10
    height = h+10
    offset = 5
    image = Image.new( 'RGB',(width,height),color=(0,0,0) )
    draw = ImageDraw.ImageDraw( image )
    draw.text((offset,offset),ch,fill=(0,255,0),font=font)
    fp = open(ch+'1.png','wb')
    image.save(fp,format='PNG')
    fp.close()
    del image
    image = Image.new( 'RGB',(width,height),color=(0,0,0) )
    draw = ImageDraw.ImageDraw( image )
    draw.text((offset,offset),ch,fill=(255,0,0),font=font)
    fp = open(ch+'2.png','wb')
    image.save(fp,format='PNG')
    fp.close()
    del image
    
if __name__ == '__main__':
    s = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    i=0
    while i<len(s):
        gen_icon(s[i])
        i+=1
