#!/usr/bin/python3

import re
from myutils.maintain import *
import sqlite3
import os

def file_input_words(filep1):
    file1=open(filep1)
    sp1=[]
    for s1 in file1:
        if len(s1)>0:
            sp2=re.split('[ \t\n\r]+',s1)
            for s2 in sp2:
                if len(s2)>0:
                    sp1+=[s2,]
    file1.close()
    return sp1

def check_exist(cursor,wd):
    sql1 = "select mean from dict where word=?"
    cu1.execute(sql1, (wd, ))
    mean=cu1.fetchone()
    if mean==None:
        return False
    else:
        return True

if __name__ == '__main__':
    db1 = sqlite3.connect( os.path.join(os.getcwd(), 'mydict.db') )
    db1.text_factory = str
    cu1 = db1.cursor()

    words = file_input_words('words.txt')
    for i in range(len(words)):
        if check_exist(cu1,words[i])==False:
            mean = baidudict(words[i])
            cu1.execute( "INSERT OR REPLACE into dict(word, mean) values( ? , ? )", (words[i], mean))
            print(words[i]+'\n')
    db1.commit()
    cu1.close()
    db1.close()