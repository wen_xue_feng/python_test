from cx_Freeze import setup,Executable
import os
#出现KeyError: 'TCL_LIBRARY'，必须设置环境变量
os.environ['TCL_LIBRARY'] = r'C:\Python\Python38-32\tcl\tcl8.6'
os.environ['TK_LIBRARY'] = r'C:\Python\Python38-32\tcl\tk8.6'
setup(
    name = '儿童识字',
    version = '1.0',
    description = '儿童识字助手',
    author ='fuhuizhong',
    author_email = 'fuhuizn@163.com',
    executables = [Executable('shizi.py',targetName='儿童识字.exe',base='Win32GUI')])