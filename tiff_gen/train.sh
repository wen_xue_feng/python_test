#!/bin/sh
#python3 tiff_gen.py
fontname="hei"
if [ -z $1 ]
then
    echo 'hei'
else
    echo $1
    fontname=$1
fi
cp output.tiff zh.${fontname}.exp0.tif
python3 rect_sharp.py > zh.${fontname}.exp0.box
tesseract zh.${fontname}.exp0.tif zh.${fontname}.exp0 nobatch box.train
unicharset_extractor zh.${fontname}.exp0.box
echo "${fontname} 0 0 0 0 0">font_properties.txt
shapeclustering -F font_properties.txt -U unicharset zh.${fontname}.exp0.tr
mftraining -F font_properties.txt -U unicharset -O ${fontname}.unicharset zh.${fontname}.exp0.tr
cntraining zh.${fontname}.exp0.tr
mv inttemp ${fontname}.inttemp
mv pffmtable ${fontname}.pffmtable
mv shapetable ${fontname}.shapetable
mv normproto ${fontname}.normproto
combine_tessdata ${fontname}
sudo mkdir -p /usr/share/tesseract-ocr/tessdata
sudo cp ${fontname}.traineddata /usr/share/tesseract-ocr/tessdata/${fontname}.traineddata
mv ${fontname}.traineddata tessdata/

