#!/usr/bin/env python3
#边缘检测
from PIL import Image

def rect_sharp(img,x,y,w,h):
    x1=x
    y1=y
    w1=w
    h1=h
    for i in range(x,x+w-1,1):
        flag = False
        for j in range(y,y+h,1):
            delta = pow( img[i+1][j]-img[i][j], 2 )
            if j>y:
                delta += pow( img[i][j]-img[i][j-1],2 )
            if delta>0:
                x1 = i
                flag = True
                break
        if flag:
            break
    for i in range(x+w-1,x1,-1):
        flag = False
        for j in range(y,y+h,1):
            delta = pow( img[i-1][j]-img[i][j], 2 )
            if j>y:
                delta += pow( img[i][j]-img[i][j-1],2 )
            if delta>0:
                w1 = i - x1+1
                flag = True
                break
        if flag:
            break
    for j in range(y,y+h-1,1):
        flag = False
        for i in range(x1,x1+w1,1):
            delta = pow( img[i][j+1]-img[i][j], 2 )
            if i>x1:
                delta += pow( img[i][j]-img[i-1][j], 2 )
            if delta>0:
                y1 = j
                flag = True
                break
        if flag:
            break
    for j in range(y+h-1,y1,-1):
        flag = False
        for i in range(x1,x1+w1,1):
            delta = pow( img[i][j-1]-img[i][j], 2 )
            if i>x1:
                delta += pow( img[i][j]-img[i-1][j], 2 )
            if delta>0:
                h1 = j - y1+1
                flag = True
                break
        if flag:
            break
    return(x1,y1,w1,h1)
    
image = [[0,0,0,0,0,0,0,0,0,0],
     [0,0,0,0,0,0,0,0,0,0],
     [0,0,0,1,0,0,0,0,0,0],
     [0,0,0,0,0,0,1,0,0,0],
     [0,0,0,0,0,0,0,0,0,0],
     [0,0,0,0,0,0,1,0,0,0],
     [0,0,0,0,0,0,0,0,0,0],
     [0,0,0,0,0,0,1,0,0,0],
     [0,0,0,0,0,0,0,0,0,0],
     [0,0,0,0,0,0,0,0,0,0]]
     
def show_rect(img,x,y,w,h):
    for j in range(y,y+h,1):
        line1 = ''
        for i in range(x,x+w,1):
            val=0
            if img[i][j]>0:
                val = '1'
            line1 = line1 + ' %s'%val
        print(line1)

def get_array(img,x1,y1,x2,y2):
    array1 = []
    for i in range(x1,x2+1,1):
        line1 = []
        for j in range(y1,y2+1,1):
            line1.append( img.getpixel( (i,j) ) )
        array1.append(line1)
    return (array1,x2-x1+1,y2-y1+1)
    
def convert_rect(x,y,rect1):
    x1 = x+rect1[0]
    y1 = y+rect1[1]
    x2 = x1+rect1[2]
    y2 = y1+rect1[3]
    return (x1,y1,x2,y2)
    
def rect_convert(img,x1,y1,x2,y2):
    x11=x1
    x21=x2
    y11=img.height-y2
    y21=img.height-y1
    return (x11,y11,x21,y21)

if __name__=='__main__':
    img = Image.open('output.tiff')
    fp = open('box','r')
    for line1 in fp:
        ln1 = line1.split()
        rect = rect_convert(img,int(ln1[1]),int(ln1[2]),int(ln1[3]),int(ln1[4]))
        img_array,w,h=get_array(img,rect[0],rect[1],rect[2],rect[3])
        rect1 = rect_sharp(img_array,0,0,w,h)
        rect = convert_rect(rect[0],rect[1],rect1)
        rect1 = rect_convert(img,rect[0],rect[1],rect[2],rect[3])
        print('%s %s %s %s %s 0'%(ln1[0],rect1[0],rect1[1],rect1[2],rect1[3]))
    #img_array,w,h=get_array(img,320,520,350,550)
    #show_rect(img_array,0,0,w,h)
    #rect = rect_sharp(img_array,0,0,w,h)
    #ret = convert_rect(320,520,rect)
    #print( ret )
    #ret=rect_convert(img,ret[0],ret[1],ret[2],ret[3])
    #print(ret)
    #ret = rect_convert(img,ret[0],ret[1],ret[2],ret[3])
    #print(ret)
    #show_rect(img_array,rect[0],rect[1],rect[2],rect[3])
