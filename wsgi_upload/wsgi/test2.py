#!/usr/bin/env python3

def app(environ,start_response):
    response_body = '\nHello my friend!\n'
    response_body = response_body.encode('utf-8')
    status = '200 OK'
    response_headers = [('Content-Type', 'text/plain'),  ('charset','UTF-8'),('Content-Length', str(len(response_body)))] 
    start_response(status, response_headers)
    return [response_body]
