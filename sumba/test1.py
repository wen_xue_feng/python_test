from numba import autojit
from time import time
class testx:
    a = 'param a'
    x = 4.3
    ret = ''
    def test2(self,n):
        for i in range(n):
            self.ret = '{0} is {1}'.format(self.a,self.x*i)
    @autojit
    def test1(self,n):
        for i in range(n):
            self.ret = '{0} is {1}'.format(self.a,self.x*i)
            
obj1 = testx()
ret = ''
def call1(a,x):
    ret = '{0} is {1}'.format(a,x)
    
def test2(n):
    x = 4.3
    ret = ''
    for i in range(n):
        ret = x*i
        
@autojit
def test1(n):
    x = 4.3
    ret = ''
    for i in range(n):
        ret = x*i

n=10000000
t1 = time()
test1(n)
t2=time()
test2(n)
t3=time()
print('no jit: {0}   use jit: {1}'.format(t3-t2,t2-t1))
t1 = time()
test1(n)
t2=time()
test2(n)
t3=time()
print('no jit: {0}   use jit: {1}'.format(t3-t2,t2-t1))
