1. 安装支持包 python3-nautilus
2. 把文件夹 nautilus-python 复制到 $HOME/.local/share/
3. 新建文件夹 $HOME/.nautilus-user-actions
4. 把文件 config.json 复制到 $HOME/.nautilus-user-actions
5. 根据需要编辑 config.json，仿照原来格式增加自定义菜单项。该文件中的 ext 字段代表文件后缀名； prog 字段代表命令；args 字段代表运行参数，args 字段中的符号 $F 代表所选的文件绝对路径，$N 代表 $F 去掉后缀名，$D 代表该文件所在的目录；label 字段代表右键菜单项名字。
