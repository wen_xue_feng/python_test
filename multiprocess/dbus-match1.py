from gi.repository import GLib
from pydbus import SessionBus

def msg_recv(sender, obj, iface, signal, params):
    print("%s %s %s %s %s" %(sender, obj, iface, signal,str(params)))
loop = GLib.MainLoop()
bus=SessionBus()
bus.subscribe(iface='test.server.hello',signal_fired=msg_recv)
loop.run()
