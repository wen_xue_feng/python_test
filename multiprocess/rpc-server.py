from xmlrpc.server import SimpleXMLRPCServer
import os,signal

class mybox:
    def __init__(self):
        self.m=[]
    def _listMethods(self):
        return ['get','put']
    def get(self):
        return self.m
    def put(self, s ):
        self.m = s
    def destroy(self):
        os.kill( os.getpid(),signal.SIGTERM )
        
server = SimpleXMLRPCServer(("localhost", 8000), allow_none=True)
server.register_instance( mybox() )
server.serve_forever()
