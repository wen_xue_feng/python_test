from gi.repository import GLib
from pydbus import SessionBus
from pydbus.generic import signal
        
class MyService( object):
    """
    <node>
        <interface name="test.server.hello">
            <method name="set">
                <arg name="value" type="s" direction="in"/>
                <arg name="response" type="s" direction="out"/>
            </method>
            <signal name="hello">
                <arg name="usr" type="s" direction="out" />
            </signal>
        </interface>
    </node>
    """
    hello = signal()
    def __init__(self):
         object.__init__(self)
    def set(self,value):
        print('set value %s'%value)
        return value
        
from threading import Thread
class sub_proc(Thread):
    def __init__(self,obj):
        Thread.__init__(self)
        self.o = obj
    def run(self):
        # 按 ENTER 发送 hello('something') 信号
        for i in range(5):
            c=input('>')
            self.o.hello(c)

def msg_recv(sender, obj, iface, signal, params):
    print("%s %s %s %s %s" %(sender, obj, iface, signal,str(params)))
loop = GLib.MainLoop()
bus=SessionBus()
obj = MyService()
bus.publish( 'test.server.hello', obj)
t = sub_proc(obj)
t.start()
loop.run()
