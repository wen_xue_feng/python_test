from multiprocessing.managers import BaseManager
from multiprocessing import Queue
import sys,signal,os,json,time
class QueueManager(BaseManager): pass
QueueManager.register('q_server')
QueueManager.register('q_client')

m = QueueManager(address=('localhost', 50000), authkey='abracadabra'.encode('utf-8'))
m.connect()
q_server = m.q_server()
q_client = m.q_client()

def recv_msg(signum, frame):
    try:
        msg = q_client.get()
        print('%s: %s'%(str(msg['from']),str(msg['value'])))
    except:
        print('q_client.get error')
    


signal.signal(signal.SIGUSR1, recv_msg)

for v in sys.argv:
    print('"%s"' %v)
print('wait response\n')
if sys.argv[1] == 'error':
    q_server.put('hello friend!\n朋友你好！')
    print('send msg')
elif sys.argv[1] == 'reg':
    for i in range(16):
        s = str( time.time() )
        msg = {'type':'r','from':s, 'to':'', 'value':os.getpid()}
        try:
            q_server.put(msg)
            time.sleep(0.3)
        except:
            print('q_server.put error')
            sys.exit(1)
    
