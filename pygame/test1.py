import pygame
from pygame.locals import *
from sys import exit
from threading import Thread,Lock
import time

#273 上
#274 下
#275 左
#276 右
#32  空格
#27  ESC

LOCK1 = Lock()
is_alive = True
def update_screen():
    global is_alive,LOCK1
    while is_alive:
        try:
            LOCK1.acquire()
            pygame.display.update()
            LOCK1.release()
        except:
            pass
        time.sleep(0.02)
    
 
pygame.init()
SCREEN_SIZE = (800, 600)
screen = pygame.display.set_mode(SCREEN_SIZE, 0, 32)
t= Thread(target=update_screen)
t.start()

font = pygame.font.Font("hei1.ttf", 18);
font_height = font.get_linesize()
event_text = []

pygame.event.set_blocked(None)
pygame.event.set_allowed( [MOUSEBUTTONDOWN,KEYDOWN,QUIT,ACTIVEEVENT] )

Fullscreen = False
message = ''
while True:
    event = pygame.event.wait()
    event_text.append(str(event))
    #获得时间的名称
    event_text = event_text[int(-SCREEN_SIZE[1]/font_height)+5:]
    #这个切片操作保证了event_text里面只保留一个屏幕的文字
 
    if event.type == QUIT:
        is_alive = False
        break
    LOCK1.acquire()
    if event.type == KEYDOWN:
        if event.key == K_f and event.mod==256:
            Fullscreen = not Fullscreen
            if Fullscreen:
                screen = pygame.display.set_mode(SCREEN_SIZE, FULLSCREEN, 32)
            else:
                screen = pygame.display.set_mode(SCREEN_SIZE, 0, 32)
        elif event.key == 273:
            message = '你按下了：上'
        elif event.key == 274:
            message = '你按下了：下'
        elif event.key == 275:
            message = '你按下了：右'
        elif event.key == 276:
            message = '你按下了：左'
        elif event.key == 32:
            message = '你按下了：空格'
        elif event.key == 27:
            message = '你按下了：ESC键'
            
    screen.fill((255, 255, 255))
    screen.blit( font.render(message, False, (0, 255, 0)), (10, font_height) )
    y = SCREEN_SIZE[1]-font_height
    #找一个合适的起笔位置，最下面开始但是要留一行的空
    for text in reversed(event_text):
        screen.blit( font.render(text, False, (0, 0, 0)), (10, y) )
        #以后会讲
        y-=font_height
        #把笔提一行
    #pygame.display.update()
    LOCK1.release()
