#!/usr/bin/env python3
#matplotlib.Font_manager.FontProperties(fname)
from pylab import *
import matplotlib.pyplot as plt
#from matplotlib.font_manager import FontProperties
import random

#用 simsum.ttf 替换了 /usr/share/matplotlib/mpl-data/fonts/ttf/DejaVuSans.ttf

#myfont = FontProperties(fname="/usr/share/fonts/truetype/arphic/ukai.ttc")
#mpl.rcParams['axes.unicode_minus'] = False

t = arange(-5*pi, 5*pi, 0.001)
y = sin(t)/t

my_post = plt.plot(t, y)
#plt.title('matplotlib中文显示测试——Tacey Wong',fontproperties=myfont)
#plt.xlabel('这里是X坐标',fontproperties=myfont)
#plt.ylabel('这里是Y坐标',fontproperties=myfont)

plt.title('matplotlib中文显示测试——Tacey Wong')
plt.xlabel('这里是X坐标')
plt.ylabel('这里是Y坐标')

plt.show()
