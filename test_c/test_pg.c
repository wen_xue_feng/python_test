#include <stdio.h>
#include <stdlib.h>
#include <libpq-fe.h>

/*
 * "dbname=fund user=user_fund password=fund001 host=127.0.0.1 port=5432"
 * */
 
void PQresultPrint(PGresult *res)
{
	int nFields = PQnfields(res);
	int nTuples = PQntuples(res);
	int i, j;
	for (i=0; i<nTuples; i++)
	{
		for (j=0; j<nFields; j++)
		{
			printf("%s  ", PQgetvalue(res, i, j));
		}
		printf("\n");
	}
}

int main(int argc, char *argv[]){
    const char *conninfo="dbname=fund user=user_fund password=fund001 host=127.0.0.1 port=5432";
    PGconn *conn;
    PGresult *res=NULL;
    char sql[200];
    conn = PQconnectdb(conninfo);
    /*Check to see how I did */
    if(PQstatus(conn) == CONNECTION_OK){
        printf("Connection succeeded.\n");
        if(argc>1){
            sprintf(sql,"select * from %s limit 10;",argv[1]);
            res = PQexec(conn, sql);
            if (PQresultStatus(res) == PGRES_TUPLES_OK)
            {
                PQresultPrint(res);
	            PQclear(res);
            }
        }
        PQfinish(conn);
        exit(0);
    }
    else
    {
        exit(-1);
    }
}
