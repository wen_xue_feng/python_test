package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

/*
#include "stdio.h"
#include "p1.h"
struct args{
	int a;
	int b;
};

static int print_args(struct args *p){
	printf("a:%d\nb:%d\n",p->a,p->b);
	return (p->a)*(p->b);
}
void set_foo(int v){
	Foo=v;
}
*/
import "C"

func runp(wg *sync.WaitGroup, str string) {
	defer wg.Done()
	t := rand.Intn(5)
	time.Sleep(time.Second * time.Duration(t))
	fmt.Println(str)
}

//export Tryme
func Tryme(str_p *C.char) {
	var arg1 = &C.struct_args{30, 80}
	C.set_foo(999)
	fmt.Println("a*b=", C.print_args(arg1), "Foo=", C.Foo)
	var str = C.GoString(str_p)
	var wg sync.WaitGroup
	for i := 1; i <= 5; i++ {
		wg.Add(1)
		go runp(&wg, fmt.Sprintf("%v %v", i, str))
	}
	wg.Wait()
}

func main() {}
