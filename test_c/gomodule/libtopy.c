#include <Python.h>
#include "libp.h"
#include "string.h"

//add wrapped function
static PyObject* wrap_Tryme(PyObject *self, PyObject *args)
{
    //Convert the python input objects into C objects
    char *s;
    if(!PyArg_ParseTuple(args, "s", &s)){
        return NULL;
    }
    //Call c function
    Tryme(s);
    //wrap the result from c function to python object.
    return (PyObject*)Py_BuildValue("s",s);
}

//define the PyMethodDef methods
static PyMethodDef wrap_methods[] ={
    {"Tryme", (PyCFunction)wrap_Tryme, METH_VARARGS,NULL},
    {NULL, NULL}
};
struct module_state {
    PyObject *error;
};
static struct PyModuleDef moduledef = {
        PyModuleDef_HEAD_INIT,
        "topy",
        NULL,
        sizeof(struct module_state),
        wrap_methods,
        NULL,
        NULL,
        NULL,
        NULL
};

//initilization function
PyMODINIT_FUNC PyInit_topy(void)
{
    PyObject *module = PyModule_Create(&moduledef);
    return module;
}
