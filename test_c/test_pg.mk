CC = gcc
CFLAGS = -Wall -I/usr/include/postgresql/
LDFLAGS = -Wall -L/usr/lib/x86_64-linux-gnu/ -lpq
OBJS = test_pg.o
TARGET = test_pg

$(TARGET):$(OBJS)
	$(CC) $(OBJS) $(LDFLAGS) -o $(TARGET) 

$(OBJS):%.o:%.c

clean:
	rm $(OBJS) $(TARGET)